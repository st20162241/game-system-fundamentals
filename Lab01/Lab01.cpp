#include <stdio.h>
#include <stdlib.h>

enum class pickup {Points, Energy, Lives, Weapon};

const int MINSCORE = 0;
const int MAXSCORE = 1000000;

int main() {
	int playerStrength = 0;
	int playerScore = 0;
	float playerX = 0.0f;
	float playerY = 0.0f;

	printf("Enter the player's strength: ");
	scanf_s("%d", &playerStrength);

	printf("Enter the player's score: ");
	while (true) {
		scanf_s("%d", &playerScore);
		bool scoreLow = (playerScore < MINSCORE);
		bool scoreHigh = (playerScore > MAXSCORE);
		if (scoreLow || scoreHigh) {
			printf("Please enter a score between 0 and 1000000");
		}
		else {
			break;
		}
	}

	printf("Enter the x coordinate for the player's position: ");
	scanf_s("%f", &playerX);

	printf("Enter the y coordinate fot the player's position: ");
	scanf_s("%f", &playerY);

	printf("Score\tStrength\tx\ty\n");
	printf("%d\t%d\t%f\t%f\n", playerScore, playerStrength, playerX, playerY);

	pickup newPickup = pickup::Lives;
}